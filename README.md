# **Support for GitLab.com issues only** 

See [Getting Help](https://about.gitlab.com/getting-help/) for support with self-hosted installations and report GitLab CE bugs in the [GitLab CE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)